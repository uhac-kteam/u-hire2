package com.unionbank.kteam.uhire;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EmployeeSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_search);
    }


    public void find(View btn) {
        Intent intent = new Intent(this, QueryActivity.class);
        startActivity(intent);
    }

}
