package com.unionbank.kteam.uhire;

import java.util.UUID;

/**
 * Created by felicito on 8/6/2016.
 */
public class Profile {
    private String ID;
    private String name;
    private String contact;
    private String detail;

    public Profile(String ID, String name, String contact, String detail) {
        this.ID = ID;
        this.name = name;
        this.contact = contact;
        this.detail = detail;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
