package com.unionbank.kteam.uhire;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);

    }

    public void getHired(View btn) {
        Intent intent = new Intent(this, EntityDetailsActivity.class);
        startActivity(intent);
    }

    public void hire(View btn) {
        Intent intent = new Intent(this, EmployeeSearchActivity.class);
        startActivity(intent);
    }
}
