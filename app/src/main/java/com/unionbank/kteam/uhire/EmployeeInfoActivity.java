package com.unionbank.kteam.uhire;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EmployeeInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_info);
    }

    public void call(View btn) {

        String number = "tel:" + "09205487289";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));

        startActivity(callIntent);
    }

}
