package com.unionbank.kteam.uhire;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EntityDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entity_details);
    }

    public void advertise(View btn) {
        Intent intent = new Intent(this, BeaconActivity.class);
        startActivity(intent);
    }
}
